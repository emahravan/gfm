/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | foam-extend: Open Source CFD
   \\    /   O peration     | Version:     4.0
    \\  /    A nd           | Web:         http://www.foam-extend.org
     \\/     M anipulation  | For copyright notice see file Copyright
-------------------------------------------------------------------------------
License
    This file is part of foam-extend.

    foam-extend is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    foam-extend is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with foam-extend.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "GFM.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //
namespace Foam
{
defineTypeNameAndDebug(GFM, 0);
defineRunTimeSelectionTable(GFM, GFM);

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

GFM::GFM
(
    volScalarField& rho1_,
    volScalarField& p1_,
    volVectorField& U1_,
    volScalarField& rhoE1_,
    volVectorField& rhoU1_,
    const basicPsiThermo&    thermoPhase1,
    volScalarField& rho2_,
    volScalarField& p2_,
    volVectorField& U2_,
    volScalarField& rhoE2_,
    volVectorField& rhoU2_,
    const basicPsiThermo&    thermoPhase2,
    const volScalarField& psi_,
    const volVectorField& n1_,
    const volVectorField& n2_,
    const STMeshHandler&  STMesh_
)
:
    IOdictionary
    (
        IOobject
        (
            "GFM",
            rho1_.time().constant(),
            rho1_.db(),
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    ),
    runTime (rho1_.time()),
    mesh    (rho1_.mesh()),
    rho1(rho1_),
    p1(p1_),
    U1(U1_),
    rhoE1(rhoE1_),
    rhoU1(rhoU1_),
    rho2(rho2_),
    p2(p2_),
    U2(U2_),
    rhoE2(rhoE2_),
    rhoU2(rhoU2_),
    psi(psi_),
    n1 (n1_),
    n2 (n2_),
    thermo1 (thermoPhase1),
    thermo2 (thermoPhase2),
    STMesh  (STMesh_),
    I1_
    (
        IOobject
        (
            "I1",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("I1",dimensionSet(0,0,0,0,0,0,0),0)
    ),
    I2_
    (
        IOobject
        (
            "I2",
            runTime.timeName(),
            mesh,
            IOobject::READ_IF_PRESENT,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("I2",dimensionSet(0,0,0,0,0,0,0),0)
    ),
    nLayers_(this->lookupOrDefault<label>("nLayers",4))
{
    updateProcFCellNum();    
}

// * * * * * * * * * * * * * * * * * Selectors * * * * * * * * * * * * * * * //

autoPtr<GFM> GFM::New
(
          volScalarField& rho1_,
          volScalarField& p1_,
          volVectorField& U1_,
          volScalarField& rhoE1_,
          volVectorField& rhoU1_,
    const basicPsiThermo&    thermoPhase1,
          volScalarField& rho2_,
          volScalarField& p2_,
          volVectorField& U2_,
          volScalarField& rhoE2_,
          volVectorField& rhoU2_,
    const basicPsiThermo&    thermoPhase2,
    const volScalarField& psi_,
    const volVectorField& n1_,
    const volVectorField& n2_,
    const STMeshHandler&  STMesh_
)
{
    word modelName;

    // Enclose the creation of the dictionary to ensure it is deleted
    // before the GFM is created otherwise the dictionary is
    // entered in the database twice
    {
        IOdictionary dict
        (
            IOobject
            (
                "GFM",
                rho1_.time().constant(),
                rho1_.db(),
                IOobject::MUST_READ,
                IOobject::NO_WRITE
            )
        );

        dict.lookup("GFM") >> modelName;
    }

    Info<< "Selecting GFM model type " << modelName << endl;

    GFMConstructorTable::iterator cstrIter =
    GFMConstructorTablePtr_->find(modelName);

    if (cstrIter == GFMConstructorTablePtr_->end())
    {
        FatalErrorIn
        (
            "GFM::New(const volScalarField&, "
            "const volVectorField&, const surfaceScalarField&, "
            "basicPsiThermo&)"
        )   << "Unknown GFM type " << modelName
            << endl << endl
            << "Valid GFM types are :" << endl
            << GFMConstructorTablePtr_->sortedToc()
            << exit(FatalError);
    }

    return autoPtr<GFM>(
                   cstrIter()(rho1_, p1_, U1_, rhoE1_, rhoU1_, thermoPhase1,  
                              rho2_, p2_, U2_, rhoE2_, rhoU2_, thermoPhase2,  
                              psi_ , n1_, n2_, STMesh_)
                   );
}

// * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * * //
//========================================================================================//        

void GFM::updateProcFCellNum()
{
    procFCellNum =List<DynamicList<label> >(mesh.V().size());
    procFPatchNum=List<DynamicList<label> >(mesh.V().size());
    
    if( Pstream::parRun() ) 
    {
        forAll ( procFCellNum , i )
        {
            procFCellNum[i](0)=-1;
            procFPatchNum[i](0)=-1;
        }
        
        forAll ( mesh.boundaryMesh() , ipatch ) 
        {
            word BCtype = mesh.boundaryMesh().types()[ipatch];
            const UList<label> &bfaceCells =  mesh.boundaryMesh()[ipatch].faceCells();
             
            if( BCtype == "processor" ) // If the boundary is of processor type procFCellNum and procFPatchNum will be changed
            {
                
                forAll( bfaceCells , icell ) 
                {
                    label P=bfaceCells[icell];
                    
                    label i=procFCellNum[P].size();
                    
                    if(procFCellNum[P][i-1]==-1)//since all are initialized to -1
                        i--;

                    procFCellNum[P](i)=icell;    
                    procFPatchNum[P](i)=ipatch;    
                }
            }
        }    
    }
}

//========================================================================================//        

void GFM::calcI()
{
    Info<<"Calculating Indicator."<<nl;

    forAll(I1_.boundaryField(),patchI)
    forAll(I1_.boundaryField()[patchI],faceI)
    {
        if(psi.boundaryField()[patchI][faceI]<0)
            I1_.boundaryField()[patchI][faceI]=1;
            
        else
            I1_.boundaryField()[patchI][faceI]=0;
    }

    forAll(I1_,Celli)
    {
        if(psi[Celli]<0.)
            I1_[Celli]=1;
        else
            I1_[Celli]=0;
    }
               
    I2_==1-I1_;

    Info<<"End of calculating Indicator."<<nl;


}

//========================================================================================//        

void GFM::mark(volScalarField& I1Copy, volScalarField& I2Copy)
{
    //marker points for fluid 1 and 2 which are adjacent to the free surface

    markers1LP_=List<DynamicList<label> >(nLayers_);
    markers2LP_=List<DynamicList<label> >(nLayers_);
    //Each processor knows its own ghost cells and their neighbour

    List<DynamicList<DynamicList<label> > > markers1LN(nLayers_);
    List<DynamicList<DynamicList<label> > > markers2LN(nLayers_);

    label etaCounter=0;

    I1_.boundaryField().evaluate();            
    I1Copy=I1_;//For ghost inside incompressible. 
    I2Copy=I1_;//For ghost inside compressible. 

    Info<<"Finding markers"<<endl;

    List< scalarField > I1CopyNeighb (mesh.boundaryMesh().size());
    List< scalarField > I2CopyNeighb (mesh.boundaryMesh().size());
    
    forAll(markers2LN,i)
    {
        I1Copy.boundaryField().evaluate();
        I2Copy.boundaryField().evaluate();
    
        if( Pstream::parRun() ) 
        {
            forAll ( mesh.boundaryMesh() , ipatch ) 
            {
                word BCtype = mesh.boundaryMesh().types()[ipatch];
             
                if( BCtype == "processor" ) 
                {
                    I1CopyNeighb[ipatch]=I1Copy.boundaryField()[ipatch].patchNeighbourField();
                    I2CopyNeighb[ipatch]=I2Copy.boundaryField()[ipatch].patchNeighbourField();
                }
            }
        }
        
        label counter1=0;
        label counter2=0;
        label procNeighbCounter=0;

        forAll(mesh.cells(),P)
        {
            
            bool markerI=false;
            if(I1Copy[P]>=1-SMALL)//If Incompressible
            {
                label neighbCounter=0;
            
                forAll(mesh.cellCells()[P],N)
                {
                    if(I1Copy[mesh.cellCells()[P][N]]<=0+SMALL)//If the neighbour is compressible
                    {
                        markers1LN[i](counter1)(neighbCounter)=mesh.cellCells()[P][N];
                        markerI=true;
                    
                        neighbCounter++;        
                        
                    }
                    
                    
                }
                
                if( Pstream::parRun() ) 
                {
                    if(!markerI)
                    {
                        if(procFCellNum[P][0]!=-1)
                        {
                            forAll(procFPatchNum[P],m)
                            {
                                label ipatch=procFPatchNum[P][m];
                                
                                if(I1CopyNeighb[ipatch][procFCellNum[P][m]] <= 0+SMALL)
                                {
                                    markerI=true;
                                }
                            }
                        }
                    }
                }
                               
                if(markerI)
                {
                    markers1LP_[i](counter1)=P;                        
                    counter1++;
                    
                }
            }
            
            bool marker=false;
            
            if(I2Copy[P]<=0+SMALL)
            {
                label neighbCounter=0;
            
                forAll(mesh.cellCells()[P],N)
                {
                 
                    if(I2Copy[mesh.cellCells()[P][N]]>=1-SMALL)
                    {                    
                        markers2LN[i](counter2)(neighbCounter)=mesh.cellCells()[P][N];
                    
                        marker=true;
                        neighbCounter++;        
                    }
                }
                
                if( Pstream::parRun() ) 
                {
                    //For incompressible I used if (if markerI do not need the operations), but he I need to count for eta, the i
                    
                    if(procFCellNum[P][0]!=-1)
                    {
                        forAll(procFPatchNum[P],m)
                        {
                         
                            label ipatch=procFPatchNum[P][m];
                                
                            if(I2CopyNeighb[ipatch][procFCellNum[P][m]] >=1-SMALL)
                            {
                                procNeighbCounter++;
                                
                                marker=true;
        
                            }
                        }
                    }
                }
                
                if(marker)
                {
                    markers2LP_[i](counter2)=P;
        
                    if(i==0)
                    {
                        
                        if(markers2LN[i].size()==counter2)
                        {
                            markers2LN[i](counter2)(0)=-1;
                        }
                        else
                            etaCounter+=markers2LN[i][counter2].size();
                        
                        if( Pstream::parRun() ) 
                        {
                            //~ etaCounter+=procNeighbCounter;
                        }
                        
                    }
                    
                    counter2++;
                }
            }
        }
        
        if(counter1>0)
        forAll(markers1LP_[i],P)
        {
            
                I1Copy[markers1LP_[i][P]]=0;
        }
        
        if(counter2>0)
        forAll(markers2LP_[i],P)
        {
                I2Copy[markers2LP_[i][P]]=1;
        }
    }
}

//========================================================================================//        
scalar GFM::fieldConstantExtrapolation(const volVectorField& N, 
                         volScalarField& v, 
                         scalar dtau, 
                   const List<DynamicList<label> >& markersP, 
                         label& iteration,
                         label iMax,
                         label i0)
{
    
    iteration++;
   
    scalar maxError=0;
     
    List< scalarField > vNeighb (mesh.boundaryMesh().size());
    List< vectorField > CNeighb (mesh.boundaryMesh().size());
        
    if( Pstream::parRun() ) 
    {
        v.boundaryField().evaluate();
        forAll ( mesh.boundaryMesh() , ipatch ) 
        {
            word BCtype = mesh.boundaryMesh().types()[ipatch];
            
            if( BCtype == "processor" ) 
            {
                vNeighb[ipatch]=v.boundaryField()[ipatch].patchNeighbourField();
                CNeighb[ipatch]=mesh.C().boundaryField()[ipatch].patchNeighbourField();
            }
        }
    }
                 
    for(label i=i0; i<iMax;i++)
    forAll(markersP[i],j)
    {
        label P=markersP[i][j];
        
        vector grad(0,0,0), C=mesh.C()[P];
             
        scalar& vn=v[P];
             
        scalar vnew=vn;
                 
        forAll(mesh.cellCells()[P],n)
        {
            label Neighb=mesh.cellCells()[P][n];
                    
            vector d=C-mesh.C()[Neighb];//d mustbe calculated in the same way grad is calculated
               
            if((d&N[P])>0)
            {
                grad+=(vn-v[Neighb])*d/(mag(d)*mag(d));
            }
            
        }
        
        if( Pstream::parRun() ) 
        {
            if(procFCellNum[P][0]!=-1)
            {
                forAll(procFPatchNum[P],m)
                {
                    label ipatch=procFPatchNum[P][m];
                    
                    scalar NeighbV=vNeighb[ipatch][procFCellNum[P][m]];
                    vector NeighbC=CNeighb[ipatch][procFCellNum[P][m]];
                    
                    vector d=C-NeighbC;//d mustbe calculated in the same way grad is calculated
                    
                    if((d&N[P])>0)
                    {
                        grad+=(vn-NeighbV)*d/(mag(d)*mag(d));
                    }
                }
            }
        }        
        
        vnew-=dtau*N[P]&grad;
                 
        scalar error=mag(vnew-vn);

        if(error>maxError)
        {
            maxError=error;
        }
            
        vn=(1-0.3)*vnew+0.3*vn;
    }
    
    return maxError;
}
  
//========================================================================================//        
  
scalar GFM::fieldConstantExtrapolation(const volVectorField& N, 
                         volVectorField& v, 
                         scalar dtau, 
                   const List<DynamicList<label> >& markersP, 
                         label& iteration, 
                         label iMax,
                         label i0)
{    
    iteration++;
    
    scalar maxError=0;
    
    List< vectorField > vNeighb (mesh.boundaryMesh().size());        
    List< vectorField > CNeighb (mesh.boundaryMesh().size());
    
    if( Pstream::parRun() ) 
    {
        v.boundaryField().evaluate();
        forAll ( mesh.boundaryMesh() , ipatch ) 
        {
            word BCtype = mesh.boundaryMesh().types()[ipatch];
            
            if( BCtype == "processor" ) 
            {
                vNeighb[ipatch]=v.boundaryField()[ipatch].patchNeighbourField();
                CNeighb[ipatch]=mesh.C().boundaryField()[ipatch].patchNeighbourField();
            }
        }
    }
                                 
    for(label i=i0; i<iMax;i++) 
    forAll(markersP[i],j)
    {
        label P=markersP[i][j];
             
        vector gradx(0,0,0); 
        vector grady(0,0,0); 
        vector gradz(0,0,0); 
                 
        vector C=mesh.C()[P];
             
        scalar& vnx=v[P].x();
        scalar& vny=v[P].y();
        scalar& vnz=v[P].z();
                
        scalar vnewx=vnx;
        scalar vnewy=vny;
        scalar vnewz=vnz;
        
        forAll(mesh.cellCells()[P],n)
        {
            label Neighb=mesh.cellCells()[P][n];
                     
            vector d=C-mesh.C()[Neighb];//d mustbe calculated in the same way grad is calculated
                 
            if((d&N[P])>0)
            {
                gradx+=(vnx-v[Neighb].x())*d/(mag(d)*mag(d));
                grady+=(vny-v[Neighb].y())*d/(mag(d)*mag(d));
                gradz+=(vnz-v[Neighb].z())*d/(mag(d)*mag(d));
            }
                 
        }
        
        if( Pstream::parRun() ) 
        {
            if(procFCellNum[P][0]!=-1)
            {
                forAll(procFPatchNum[P],m)
                {
                    label ipatch=procFPatchNum[P][m];
                    
                    vector NeighbV=vNeighb[ipatch][procFCellNum[P][m]];
                    vector NeighbC=CNeighb[ipatch][procFCellNum[P][m]];
                    
                    vector d=C-NeighbC;//d mustbe calculated in the same way grad is calculated
                    
                    if((d&N[P])>0)
                    {
                        gradx+=(vnx-NeighbV.x())*d/(mag(d)*mag(d));
                        grady+=(vny-NeighbV.y())*d/(mag(d)*mag(d));
                        gradz+=(vnz-NeighbV.z())*d/(mag(d)*mag(d));
                    }         
                    
                }
            }
        }
                        
        vnewx-=dtau*N[P]&gradx;     
        vnewy-=dtau*N[P]&grady;     
        vnewz-=dtau*N[P]&gradz;     
            
        scalar errorx=mag(vnewx-vnx);
        scalar errory=mag(vnewy-vny);
        scalar errorz=mag(vnewz-vnz);

        if(errorx>maxError)
        {
            maxError=errorx;
        }
        if(errory>maxError)
        {
            maxError=errory;
        }
        if(errorz>maxError)
        {
            maxError=errorz;
        }
        scalar alpha = 0.3;        
        
        vnx=(1-alpha)*vnewx+alpha*vnx;
        vny=(1-alpha)*vnewy+alpha*vny;
        vnz=(1-alpha)*vnewz+alpha*vnz;
        
    }
    return maxError;
    
}

// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //
} // End namespace Foam


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// ************************************************************************* //
