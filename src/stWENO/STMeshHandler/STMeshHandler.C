#include "STMeshHandler.H"

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::STMeshHandler::STMeshHandler(const fvMesh& mesh_,scalar deltaX):
    mesh(mesh_),
    NStMesh_
    (
        IOobject
        (
            "NStMesh",
            runTime.constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    ),
    oneD_(check1D()),
    twoD_(check2D()),
    threeD_(check3D()),
    runTime(mesh.time()),
    counterRec( List<label>(Pstream::nProcs(),0) ),
    counterSend( List<label>(Pstream::nProcs(),0) ),
    IJK_(List<Vector<label>> (mesh.V().size())),
    XYZ_(scalarField(NStMesh_[0],-1),scalarField(NStMesh_[1],-1),scalarField(NStMesh_[2],0)),
    deltaX_(deltaX),
    sendCells(List<DynamicList<label> >(Pstream::nProcs())  ),
    IJKRec(List<DynamicList<Vector<label> > >(Pstream::nProcs())  ),
    NRequired(0)
    {
        Pout<<"Creating STMeshHandler object."<<endl;
        
        Pout<<"Reading constant/structuredMesh."<<endl;
        std::ifstream file;
        file.open("constant/structuredMesh");

        if(!threeD())
        for(label i=0; i<NStMesh_[0];i++)
        for(label j=0; j<NStMesh_[1];j++)
        {    
            file>>XYZ_.x()[i]>>XYZ_.y()[j];
        }    
        else
        {
            scalar x0=min(mesh.C().internalField().component(0));
            scalar y0=min(mesh.C().internalField().component(1));
            scalar z0=min(mesh.C().internalField().component(2));

            reduce(x0, minOp<scalar>());
            reduce(y0, minOp<scalar>());
            reduce(z0, minOp<scalar>());

            for(label i=0; i<NStMesh_[0];i++)
            {
                XYZ_.x()[i]=x0+i*deltaX_;
            }
            for(label j=0; j<NStMesh_[1];j++)
            {
                XYZ_.y()[j]=y0+j*deltaX_;
            }
            for(label k=0; k<NStMesh_[2];k++)
            {    
                XYZ_.z()[k]=z0+k*deltaX_;;
            }   
        }
        file.close();
        
        findIJK();

        if( Pstream::parRun() ) 
        {
            collectRequired();
        }
    }

//========================================================================================//        

void Foam::STMeshHandler::collectRequired()
{
    Pout<<"Collecting required cells for receiving and sending."<<endl;
    
    List<DynamicList<Vector<label> > > IJKRecieve(Pstream::nProcs());
    List<List<DynamicList<label> > > recieveProcessers(Pstream::nProcs(), List<DynamicList<label> >(Pstream::nProcs()));
    List<List<DynamicList<label> > > cellNumInRecieveProcessers(Pstream::nProcs(), List<DynamicList<label> >(Pstream::nProcs()));
    List<DynamicList<label> > ReqCells (List<DynamicList<label> >(Pstream::nProcs())  );
    
    label N=Pstream::myProcNo();
    
    Pout<<"Processor number is preserved in N: "<<N<<endl;
    
    label counter=0;
    
    Pout<<"Constructing a list of IJK which are needed from other processes."<<endl;
    
    for(label il=0; il<ijkCell().size(); il++)
    for(label jl=0; jl<ijkCell()[il].n(); jl++)
    for(label kl=0; kl<ijkCell()[il].m(); kl++)
    {
        if(ijkCell()[il][jl][kl]==-1)
        {
            IJKRecieve[N](counter)=Vector<label> (globalIndex(il,vector::X),globalIndex(jl,vector::Y),globalIndex(kl,vector::Z));
            counter++;
        }
    }
    
    
    Pout<<"gathering and scattering the list, so that all processes know which processor needs what IJK."<<endl;
    
    Pstream::gatherList(IJKRecieve);
    Pstream::scatterList(IJKRecieve);
    
    Pout<<"Now search through all the list and see which processors need this processor. If any processor does not contain the required ijk cell, I still allocate recieveProcessers and cellNumInRecieveProcessers for it, but it contains -1. Later I will find associated processors by checking if the value is not -1."<<endl;
    
    forAll(IJKRecieve,c)
    {
        if(c!=Pstream::myProcNo())
        {
            forAll(IJKRecieve[c],P)
            {
                recieveProcessers[N][c](P)=-1;
                cellNumInRecieveProcessers[N][c](P)=-1;
                Vector<label> ijk=IJKRecieve[c][P];
    
                if( ijkMin().x()<=ijk.x() && ijkMin().y()<=ijk.y() &&ijkMin().z()<=ijk.z() && 
                    ijkMax().x()>=ijk.x() && ijkMax().y()>=ijk.y() &&ijkMax().z()>=ijk.z()  )
                {
                    Vector<label> ijkl=localIndex(ijk);
                    
                    if(ijkCell_[ijkl.x()][ijkl.y()][ijkl.z()]!=-1)
                    {
                        recieveProcessers[N][c][P]=N;
                        cellNumInRecieveProcessers[N][c][P]=ijkCell()[ijkl.x()][ijkl.y()][ijkl.z()];
                    }
                }
            }
        }
    }
    forAll(recieveProcessers[N],c)
    {
        Pout<<"recieveProcessers, size: "<<recieveProcessers[N][c].size()<<endl;
    }
    Pout<<"Gathering and scattering recieveProcessers and cellNumInRecieveProcessers."<<endl;
    
    Pstream::gatherList(recieveProcessers);
    Pstream::scatterList(recieveProcessers);
    
    Pstream::gatherList(cellNumInRecieveProcessers);
    Pstream::scatterList(cellNumInRecieveProcessers);
    
    Pout<<"Constructing IJKRec and ReqCells for each process. IJKRec is the IJK of the required cell and cellNumInRecieveProcessers is the number of the cell in that processor. "<<endl;
    
    forAll(IJKRecieve[N],P)
    {
        forAll(recieveProcessers,c)
        {
            if(c!=Pstream::myProcNo())
            if(recieveProcessers[c][N][P]!=-1)
            {
                IJKRec[c](counterRec[c])=IJKRecieve[N][P];
                ReqCells[c](counterRec[c])=cellNumInRecieveProcessers[c][N][P];
                counterRec[c]++;
            }
        }
    }
    
    Pout<<"Now we send ReqCells of each processor to others, and they will keep the list in sendCell, so that they now what to send for which processor. counterRec and counterSend also give the size of the sendCell. This is due to the fact that sendCell may be empty and not constructed. In such a case return value of size() function will be a random number."<<endl;
    //~ Pout<<"ReqCells: "<<ReqCells<<endl;
    Pout<<"counter: "<<counter<<endl;
    Pout<<"counterRec: "<<counterRec<<endl;
    
    for(label pn=0; pn<Pstream::nProcs();pn++)
    {
        if(Pstream::myProcNo()==pn)
        {
            for(label pnOther=0; pnOther<Pstream::nProcs();pnOther++)
            {
                if(Pstream::myProcNo()!=pnOther)
                {
                    Pout<<"Sending to "<<pnOther<<endl;
                    {
                        OPstream toOther(Pstream::scheduled, pnOther); 
                        toOther << ReqCells[pnOther];
                    }
                    {
                        OPstream toOther(Pstream::scheduled, pnOther); 
                        toOther << counterRec[pnOther];
                    }
                }
            }
        }
        else
        {
            Pout<<"recieving from "<<pn<<endl;
            {
                IPstream fromOther(Pstream::scheduled, pn);
                fromOther >> sendCells[pn];
            }
            
            {
                IPstream fromOther(Pstream::scheduled, pn);
                fromOther >> counterSend[pn];
            }
        }
    }
}


//========================================================================================//        

template <class T, class W>
void Foam::STMeshHandler::updateStField(const T& v, List<RectangularMatrix<W> >& stv ) const
{
    List<Field<W> > vRec(Pstream::nProcs());
    List<Field<W> > vSend(Pstream::nProcs());
    
    forAll(v,P)
    {
        Vector<label> ijk=localIndex(IJK_[P]);
        stv[ijk.x()][ijk.y()][ijk.z()]=v[P];
    }
    
    for(label pn=0; pn<Pstream::nProcs();pn++)
    {
        if(Pstream::myProcNo()!=pn && counterSend[pn]!=0)
        {
            vSend[pn]=List<W>(counterSend[pn]);
            
            forAll(vSend[pn],l)
            {
                label P=sendCells[pn][l];
                
                vSend[pn][l]=v[P];
            }
        }
    }
    
    for(label pn=0; pn<Pstream::nProcs();pn++)
    {
        if(Pstream::myProcNo()==pn)
        {
            for(label pnOther=0; pnOther<Pstream::nProcs();pnOther++)
            {
                if(Pstream::myProcNo()!=pnOther)
                {
                    OPstream toOther(Pstream::scheduled, pnOther); 
                    toOther << vSend[pnOther];
                }
            }
        }
        else
        {
            vRec[pn]=List<W>(counterRec[pn]);
            IPstream fromOther(Pstream::scheduled, pn);
            fromOther >> vRec[pn];
        }
    }
    
    
    for(label pn=0; pn<Pstream::nProcs();pn++)
    {
        if(Pstream::myProcNo()!=pn)
        {
            forAll(vRec[pn],l)
            {
                label i=localIndex(IJKRec[pn][l].x(),vector::X);
                label j=localIndex(IJKRec[pn][l].y(),vector::Y);
                label k=localIndex(IJKRec[pn][l].z(),vector::Z);

                stv[i][j][k]=vRec[pn][l];
            }
        }
    }
}

//========================================================================================//        

void STMeshHandler::findIJK() 
{
    const scalar threshold=deltaX_/1000.;
    
    Pout<<"Finding IJK."<<endl;
    ijkMin_=Vector<label>(100000,100000,100000);
    ijkMax_=Vector<label>(0,0,0);
    
    scalar xMin=XYZ_.x()[0];
    scalar yMin=XYZ_.y()[0];
    scalar zMin=XYZ_.z()[0];
    
    forAll(IJK_,Celli)
    {
        bool found=false;
        label i0=(mesh.C()[Celli].x()-xMin+threshold)/deltaX_;
        label iN=i0;
        
        label j0=(mesh.C()[Celli].y()-yMin+threshold)/deltaX_;
        label jN=j0;
        
        label k0=(mesh.C()[Celli].z()-zMin+threshold)/deltaX_;
        label kN=k0;
        
        for(label i=i0; i<=iN; i++)
        {
            for(label j=j0; j<=jN; j++)
            {   
                for(label k=k0; k<=kN; k++)
                {   

                    if( Foam::mag(XYZ_.x()[i]-mesh.C()[Celli].x())<threshold )
                    if( Foam::mag(XYZ_.y()[j]-mesh.C()[Celli].y())<threshold )
                    if( Foam::mag(XYZ_.z()[k]-mesh.C()[Celli].z())<threshold )
                    {
                        if(i<ijkMin_.x())
                        {
                            ijkMin_.x()=i;
                        }
                        if(i>ijkMax_.x())
                        {
                            ijkMax_.x()=i;
                        }
                        if(j<ijkMin_.y())
                        {
                            ijkMin_.y()=j;
                        }
                        if(j>ijkMax_.y())
                        {
                            ijkMax_.y()=j;
                        }
                        if(k<ijkMin_.z())
                        {
                            ijkMin_.z()=k;
                        }
                        if(k>ijkMax_.z())
                        {
                            ijkMax_.z()=k;
                        }
                        
                        IJK_[Celli]=Vector<label>(i,j,k);
                 
                        found=true;

                        break;
                    }
                 
                }
                if(found)
                    break;
            }
        }
        if(!found)
        {
            Info<<"Not Fouuuuuuuuuuuuuuuund"<<endl;
            ::exit(0);
        }
    }
    Pout<<"Before correcting: iMax="<<ijkMax_.x()<<"     iMin="<<ijkMin_.x()<<"      jMax="<<ijkMax_.y()<<"     jMin="<<ijkMin().y()<<"      kMax="<<ijkMax_.z()<<"     kMin="<<ijkMin().z()<<endl;
    if(ijkMin().x()>0)
    {
        ijkMin_.x()-=3;
    }
    if(ijkMax().x()<NStMesh_[0]-1)
    {
        ijkMax_.x()+=3;
    }

    if(ijkMax().y()-ijkMin().y()>0)
    {
        if(ijkMin().y()>0)
        {
            ijkMin_.y()-=3;
        }
        if(ijkMax().y()<(NStMesh_[1]-1))
        {
            ijkMax_.y()+=3;
        }
    }
    if(ijkMax_.z()-ijkMin().z()>0)
    {
        if(ijkMin().z()>0)
        {
            ijkMin_.z()-=3;
        }
        if(ijkMax().z()<(NStMesh_[2]-1))
        {
            ijkMax_.z()+=3;
        }
    }
    Pout<<"Marking -1, any cell in range which is not for this processor."<<endl;
    
    Pout<<"ijkMax="<<ijkMax_<<endl;
    
    const label iSize=ijkMax().x()-ijkMin().x()+1;
    const label jSize=ijkMax().y()-ijkMin().y()+1;
    const label kSize=ijkMax().z()-ijkMin().z()+1;
    ijkCell_=List<RectangularMatrix<label> >  (iSize, RectangularMatrix<label> (jSize,kSize,-1)) ;
    
    forAll(IJK_,Celli)
    {
        Vector<label> ijk=localIndex(IJK_[Celli]);
                
        ijkCell_[ijk.x()][ijk.y()][ijk.z()]=Celli;
    }
    
    NRequired=(ijkMax().x()-ijkMin().x()+1)*(ijkMax().y()-ijkMin().y()+1)*(ijkMax().z()-ijkMin().z()+1)-IJK().size();
    
    Pout<<"end of searching for IJK."<<endl;
}

//========================================================================================//        
template <class W>
W Foam::STMeshHandler::bilinearInterpolation2D(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<W> >& stv,const vector X ) const
{
    List<W> f(2); 
    
    label counter=0;
    const label k=0;
    for(label P=0; P<=2; P+=2)
    {
        label iR0=cellIJK[P].x(),iR1=cellIJK[P+1].x();
        label jR0=cellIJK[P].y(),jR1=cellIJK[P+1].y();
        
        label i0=localIndex(iR0,vector::X),i1=localIndex(iR1,vector::X);
        label j0=localIndex(jR0,vector::Y),j1=localIndex(jR1,vector::Y);
        
        f[counter]=(XYZ_.x()[iR1]-X.x())/(XYZ_.x()[iR1]-XYZ_.x()[iR0])*stv[i0][j0][k]
          +(XYZ_.x()[iR0]-X.x())/(XYZ_.x()[iR0]-XYZ_.x()[iR1])*stv[i1][j1][k];
    
        counter++;
    }
    
    label jR0=cellIJK[0].y(),jR1=cellIJK[2].y();
          
    return (XYZ_.y()[jR1]-X.y())/(XYZ_.y()[jR1]-XYZ_.y()[jR0])*f[0]
          +(XYZ_.y()[jR0]-X.y())/(XYZ_.y()[jR0]-XYZ_.y()[jR1])*f[1];
}
//========================================================================================//        
template <class W>
W STMeshHandler::bilinearInterpolation3D(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<W> >& stv,const vector X ) const
{
    List<W> f1(2); 
    List<W> f2(2); 
    List<W> f(2); 
    
    label counter=0;
    
    for(label P=0; P<=2; P+=2)
    {
        label iR0=cellIJK[P].x(),iR1=cellIJK[P+1].x();
        label jR0=cellIJK[P].y(),jR1=cellIJK[P+1].y();
        label kR0=cellIJK[P].z(),kR1=cellIJK[P+1].z();
        
        label i0=localIndex(iR0,vector::X), i1=localIndex(iR1,vector::X);
        label j0=localIndex(jR0,vector::Y), j1=localIndex(jR1,vector::Y);
        label k0=localIndex(kR0,vector::Z), k1=localIndex(kR1,vector::Z);
        
        f1[counter]=(XYZ_.x()[iR1]-X.x())/(XYZ_.x()[iR1]-XYZ_.x()[iR0])*stv[i0][j0][k0]
          +(XYZ_.x()[iR0]-X.x())/(XYZ_.x()[iR0]-XYZ_.x()[iR1])*stv[i1][j1][k1];
    
        counter++;
    }
    
    counter=0;
    
    for(label P=4; P<=6; P+=2)
    {
        label iR0=cellIJK[P].x(),iR1=cellIJK[P+1].x();
        label jR0=cellIJK[P].y(),jR1=cellIJK[P+1].y();
        label kR0=cellIJK[P].z(),kR1=cellIJK[P+1].z();
        
        label i0=localIndex(iR0,vector::X), i1=localIndex(iR1,vector::X);
        label j0=localIndex(jR0,vector::Y), j1=localIndex(jR1,vector::Y);
        label k0=localIndex(kR0,vector::Z), k1=localIndex(kR1,vector::Z);
        
        f2[counter]=(XYZ_.x()[iR1]-X.x())/(XYZ_.x()[iR1]-XYZ_.x()[iR0])*stv[i0][j0][k0]
          +(XYZ_.x()[iR0]-X.x())/(XYZ_.x()[iR0]-XYZ_.x()[iR1])*stv[i1][j1][k1];
    
        counter++;
    }
    
    label kR0=cellIJK[0].z(),kR1=cellIJK[4].z();
    
    f[0]=(XYZ_.z()[kR1]-X.z())/(XYZ_.z()[kR1]-XYZ_.z()[kR0])*f1[0]
          +(XYZ_.z()[kR0]-X.z())/(XYZ_.z()[kR0]-XYZ_.z()[kR1])*f1[1];
    
    f[1]=(XYZ_.z()[kR1]-X.z())/(XYZ_.z()[kR1]-XYZ_.z()[kR0])*f2[0]
          +(XYZ_.z()[kR0]-X.z())/(XYZ_.z()[kR0]-XYZ_.z()[kR1])*f2[1];
    
    label jR0=cellIJK[0].y(),jR1=cellIJK[2].y();
    
    return (XYZ_.y()[jR1]-X.y())/(XYZ_.y()[jR1]-XYZ_.y()[jR0])*f[0]
          +(XYZ_.y()[jR0]-X.y())/(XYZ_.y()[jR0]-XYZ_.y()[jR1])*f[1];
}

//======================================================================================================================================//
template <class W>
W STMeshHandler::shepardInterpolation(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<W> >& stv,const vector X ) const
{
    W num=0*stv[0][0][0];
    scalar dNum=0;
    
    forAll(cellIJK,P)
    {
        label i=localIndex(cellIJK[P].x(),vector::X),
              j=localIndex(cellIJK[P].y(),vector::Y),
              k=localIndex(cellIJK[P].z(),vector::Z);
              
        scalar mult=1;

        label iR=cellIJK[P].x();
        label jR=cellIJK[P].y();
        label kR=cellIJK[P].z();
        
        mult=1./( (X.x()-XYZ_.x()[iR])*(X.x()-XYZ_.x()[iR])
                 +(X.y()-XYZ_.y()[jR])*(X.y()-XYZ_.y()[jR]) 
                 +(X.z()-XYZ_.z()[kR])*(X.z()-XYZ_.z()[kR]) 
                 );
        
        num+=mult*stv[i][j][k];
        dNum+=mult;
    }
    return num/dNum;
}
//========================================================================================//        
List<Vector<label> > Foam::STMeshHandler::findSurroundingCells2D(const label P,const vector X) const
{
    label i=IJK_[P].x();
    label j=IJK_[P].y();
    
    List<Vector<label> > Cells(4);
    
    label iIncrement=1;
    label jIncrement=1;
    //~ Info<<"!!! i="<<i<<endl;
    //~ Info<<"x="<<X.x()<<"    xi="<<XYZ_.x()[i]<<"    xj="<<XYZ_.y()[j]<<endl;
    
    if(X.x()>XYZ_.x()[i])
    {
        do
        {
            i+=1;
            //~ Info<<"i="<<i<<"    x="<<X.x()<<"    xi="<<XYZ_.x()[i]<<endl;
            
            if(X.x()<=XYZ_.x()[i] && X.x()>=XYZ_.x()[i-1])
            {
                break;
            }
            
        }while(i<XYZ_.x().size()-1&&i>=0);
    }
    
    else
    {
        iIncrement=-1;
        do
        {
            i-=1;
            //~ Info<<"i="<<i<<"    x="<<X.x()<<"    xi="<<XYZ_.x()[i]<<endl;
            if(X.x()>=XYZ_.x()[i] && X.x()<=XYZ_.x()[i+1])
            {
                break;
            }
            
        }while(i<XYZ_.x().size()-1&&i>=0);
   }
   
   if(X.y()>XYZ_.y()[j])
   {
        do
        {
            j+=1;
            if(X.y()<=XYZ_.y()[j] && X.y()>=XYZ_.y()[j-1])
                break;
        }while(j<XYZ_.y().size()-1&&j>=0);
    }
    else
    {
        jIncrement=-1;
        do
        {
            j-=1;
            if(X.y()>=XYZ_.y()[j] && X.y()<=XYZ_.y()[j+1])
                break;
        }while(j<XYZ_.y().size()-1&&j>=0);
       
    } 
    Cells[0]=Vector<label>(i,j,0);
    Cells[1]=Vector<label>(i-iIncrement,j,0);
    Cells[2]=Vector<label>(i,j-jIncrement,0);
    Cells[3]=Vector<label>(i-iIncrement,j-jIncrement,0);
    
    return Cells;
}
//========================================================================================//        

List<Vector<label> > STMeshHandler::findSurroundingCells3D(const label P,const vector X) const
{
    label i=IJK_[P].x();
    label j=IJK_[P].y();
    label k=IJK_[P].z();

    List<Vector<label> > Cells(8);
    
    label iIncrement=1;
    label jIncrement=1;
    label kIncrement=1;    
    
    if(X.x()>XYZ_.x()[i])
    {
        do
        {
            i+=1;
            
            if(X.x()<=XYZ_.x()[i] && X.x()>=XYZ_.x()[i-1])
            {
                break;
            }
            
        }while(i<XYZ_.x().size()-1&&i>=0);
    }
    
    else
    {
        iIncrement=-1;
        do
        {
            i-=1;

            if(X.x()>=XYZ_.x()[i] && X.x()<=XYZ_.x()[i+1])
            {
                break;
            }
            
        }while(i<XYZ_.x().size()-1&&i>=0);
   }
   
   if(X.y()>XYZ_.y()[j])
   {
        do
        {
            j+=1;
            if(X.y()<=XYZ_.y()[j] && X.y()>=XYZ_.y()[j-1])
                break;
        }while(j<XYZ_.y().size()-1&&j>=0);
    }
    else
    {
        jIncrement=-1;
        do
        {
            j-=1;
            if(X.y()>=XYZ_.y()[j] && X.y()<=XYZ_.y()[j+1])
                break;
        }while(j<XYZ_.y().size()-1&&j>=0);
       
    } 
   
   if(X.z()>XYZ_.z()[k])
   {
        do
        {
            k+=1;
            if(X.z()<=XYZ_.z()[k] && X.z()>=XYZ_.z()[k-1])
                break;
        }while(k<XYZ_.z().size()-1&&k>=0);        
    }
    else
    {
        kIncrement=-1;
        do
        {
            k-=1;
            if(X.z()>=XYZ_.z()[k] && X.z()<=XYZ_.z()[k+1])
                break;
        }while(k<XYZ_.z().size()-1&&k>=0);        
    } 
    
    Cells[0]=Vector<label>(i,j,k);
    Cells[1]=Vector<label>(i-iIncrement,j,k);
    Cells[2]=Vector<label>(i,j-jIncrement,k);
    Cells[3]=Vector<label>(i-iIncrement,j-jIncrement,k);
    Cells[4]=Vector<label>(i,j,k-kIncrement);
    Cells[5]=Vector<label>(i-iIncrement,j,k-kIncrement);
    Cells[6]=Vector<label>(i,j-jIncrement,k-kIncrement);
    Cells[7]=Vector<label>(i-iIncrement,j-jIncrement,k-kIncrement);
    return Cells;
}


//Template functions explicit instantiation
template Foam::scalar Foam::STMeshHandler::bilinearInterpolation2D<scalar>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<scalar> >& stv,const vector X ) const;
template Foam::vector Foam::STMeshHandler::bilinearInterpolation2D<vector>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<vector> >& stv,const vector X ) const;
template Foam::tensor Foam::STMeshHandler::bilinearInterpolation2D<tensor>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<tensor> >& stv,const vector X ) const;


template Foam::scalar STMeshHandler::bilinearInterpolation3D<scalar>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<scalar> >& stv,const vector X ) const;
template Foam::vector STMeshHandler::bilinearInterpolation3D<vector>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<vector> >& stv,const vector X ) const;
template Foam::tensor STMeshHandler::bilinearInterpolation3D<tensor>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<tensor> >& stv,const vector X ) const;

template Foam::scalar STMeshHandler::shepardInterpolation<scalar>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<scalar> >& stv,const vector X ) const;
template Foam::vector STMeshHandler::shepardInterpolation<vector>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<vector> >& stv,const vector X ) const;
template Foam::tensor STMeshHandler::shepardInterpolation<tensor>(const List<Vector<label> >& cellIJK,const List<RectangularMatrix<tensor> >& stv,const vector X ) const;


template void Foam::STMeshHandler::updateStField<Foam::volScalarField, scalar>(const volScalarField& v, List<RectangularMatrix<scalar> >& stv ) const;
template void Foam::STMeshHandler::updateStField<Foam::volVectorField, vector>(const volVectorField& v, List<RectangularMatrix<vector> >& stv ) const;
template void Foam::STMeshHandler::updateStField<Foam::volTensorField, tensor>(const volTensorField& v, List<RectangularMatrix<tensor> >& stv ) const;
