This is a library for the ghost fluid method (GFM) written within foam-extend 4.0.
It is relying on an orthogonal structured mesh, handled through STMeshHandler class. 
There has been many changes in the library in recent days. The code has bug in GFM library that causes failure in the tutorials. 

Three solvers are available:

eDbnsFoam: 
      Single-phase compressible solver similar to dbnsFoam, but using e.

GFMCompCompFoam:
      Compressible-compressible solver. eDbnsFoam is used for compressible phases. Level set (5th orther WENO,3rd order TV-RK) is used for capturing the interface

GFMCompIncompFoam: 
      Compressible-incompressible solver. eDbnsFoam is used for compressible phase, icoFoam for incomressible. Level set (5th orther WENO,3rd order TV-RK) is used for capturing the interface
	  This solve is outdated, it should be upgraded to use stWENO and GFM libraries.

Utilities:
    createStMesh: Should be run after blockMesh for each tutorials

src: 
    thermophysical library is modified to account for the phaseName and multiple functions are added to calculate properties, e.g. entropy, sound velocity, etc.
	dbns library is updated to be linked with thermphysical library instead of hardcoded ideal gas equations.