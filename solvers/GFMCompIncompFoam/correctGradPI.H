{
    const unallocLabelList& own = mesh.owner();
    const unallocLabelList& nei = mesh.neighbour();
    DynamicList<label>& markersIP = markersILP[0];
    
    scalar sigmaK=0;
    
    List< scalarField > sigmakNeighb (mesh.boundaryMesh().size());
    List< scalarField > psiNeighb (mesh.boundaryMesh().size());
    List< vectorField > CNeighb (mesh.boundaryMesh().size());
    List< vectorField > nNeighb (mesh.boundaryMesh().size());
    List< scalarField > INeighb (mesh.boundaryMesh().size());
    
    if( Pstream::parRun() ) 
    {
        forAll ( mesh.boundaryMesh() , ipatch ) 
        {
            word BCtype = mesh.boundaryMesh().types()[ipatch];
            
            if( BCtype == "processor" ) 
            {
                sigmakNeighb[ipatch]=sigmak.boundaryField()[ipatch].patchNeighbourField();
                psiNeighb[ipatch]=psi.boundaryField()[ipatch].patchNeighbourField();
                CNeighb[ipatch]=mesh.C().boundaryField()[ipatch].patchNeighbourField();
                INeighb[ipatch]=I.boundaryField()[ipatch].patchNeighbourField();
                nNeighb[ipatch]=n.boundaryField()[ipatch].patchNeighbourField();
            }
        }
    }
    
    forAll(markersIP,i)
    {
        label P=markersIP[i];
        const labelList& faceLabels = mesh.cells()[P];
    
        forAll(faceLabels,j)
        {
            label f=faceLabels[j];
            
            label N=own[f];
            
            vector SF=-mesh.Sf()[f];
            
            if (mesh.isInternalFace(f))
            {
                if (P == N)
                {  
                    N=nei[f];
                    SF=-SF;
                }
      
                if(I[N]<=0+SMALL)
                {
                    vector nEta=( n[N]*fabs(psi[P])+n[P]*fabs(psi[N]) )/( fabs(psi[N])+fabs(psi[P]) );
                    
                    if( std::fabs(nEta.y())>std::fabs(nEta.x()) )//hirizontal interface
                    {
                        if(std::fabs(mesh.C()[P].x()-mesh.C()[N].x())<1e-12)
                        {
                             if(std::fabs(psi[P])<std::fabs(psi[N]))
                             {
                                 sigmaK=sigmak[P];
                             }
                             else
                             {
                                 sigmaK=sigmak[N];
                             }
                        }
                        else
                        {
                            sigmaK=( sigmak[N]*fabs(psi[P])+sigmak[P]*fabs(psi[N]) )/( fabs(psi[N])+fabs(psi[P]) );
                        }
                    }
                    else
                    {
                        if(std::fabs(mesh.C()[P].y()-mesh.C()[N].y())<1e-12)//Vertical interface
                        {
                             if(std::fabs(psi[P])<std::fabs(psi[N]))
                             {
                                 sigmaK=sigmak[P];
                             }
                             else
                             {
                                 sigmaK=sigmak[N];
                             }
                        }
                        else
                        {
                            sigmaK=( sigmak[N]*fabs(psi[P])+sigmak[P]*fabs(psi[N]) )/( fabs(psi[N])+fabs(psi[P]) );
                        }
                    }

                    scalar Jump=sigmaK/rhoI.value();
                    
                    gradPI[P]+=Jump*SF/mesh.V()[P]*mesh.deltaCoeffs()[f]*Foam::mag(mesh.C()[P]-mesh.Cf()[f]);
                    
                }
            }
        }
        
        if( Pstream::parRun() ) 
        {
            
            if(procFCellNum[P][0]!=-1)
            {
                forAll(procFPatchNum[P],m)
                {
                    
                    label ipatch=procFPatchNum[P][m];
                    label PFN=procFCellNum[P][m];
                    
                    if(INeighb[ipatch][PFN]<=0+SMALL)
                    {                                       
                        scalar psiNei=psiNeighb[ipatch][PFN];
            
                        scalar sigmakNei=sigmakNeighb[ipatch][PFN];
            
                        vector CNei=CNeighb[ipatch][PFN];
                        
                        vector nNei=nNeighb[ipatch][PFN];

                        vector nEta=( nNei*fabs(psi[P])+n[P]*fabs(psiNei) )/( fabs(psiNei)+fabs(psi[P]) );

                        if( std::fabs(nEta.y())>std::fabs(nEta.x()) )
                        {
                            if(std::fabs(mesh.C()[P].x()-CNei.x())<1e-12)
                            {
                                if(std::fabs(psi[P])<std::fabs(psiNei))
                                {
                                    sigmaK=sigmak[P];
                                }
                                else
                                {
                                    sigmaK=sigmakNei;
                                }
                            }
                            else
                            {
                                sigmaK=( sigmakNei*fabs(psi[P])+sigmak[P]*fabs(psiNei) )/( fabs(psiNei)+fabs(psi[P]) );
                            }
                        }
                        
                        else
                        {
                            if(std::fabs(mesh.C()[P].y()-CNei.y())<1e-12)
                            {
                                if(std::fabs(psi[P])<std::fabs(psiNei))
                                {
                                    sigmaK=sigmak[P];
                                }
                                else
                                {
                                    sigmaK=sigmakNei;
                                }
                            }
                            else
                            {
                                sigmaK=( sigmakNei*fabs(psi[P])+sigmak[P]*fabs(psiNei) )/( fabs(psiNei)+fabs(psi[P]) );
                            }
                        }
                        
                        label f=0;
                        
                        vector d=(CNei-mesh.C()[P]);
                        vector df=vector(0,0,0);
                        
                        forAll(faceLabels,j)
                        {
                            f=faceLabels[j];
                            
                            if (!mesh.isInternalFace(f))
                            {
                                
                                label BoundaryLabel=mesh.boundaryMesh().whichPatch(f);
                        
                                if(BoundaryLabel==ipatch )
                                {
                                    df=mesh.Cf()[f]-mesh.C()[P];
                                    if(( (d&df)/(mag(d)*mag(df)) ) >= 1.-1e-12)
                                    {
                                        break;
                                    }
                                }

                            }
                        }
                        
                        vector SF=mesh.Sf()[f];
                        
                        if ((SF&d)<0)
                        {  
                            SF=-SF;
                        }
                                                
                        scalar Jump=sigmaK/rhoI.value();
                        
                        gradPI[P]+=Jump*SF/mesh.V()[P]/mag(d)*Foam::mag(mesh.C()[P]-mesh.Cf()[f]);
                        
                    }
                }
            }
        }        
    }
}
