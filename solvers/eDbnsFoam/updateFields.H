{
    // Compute U
    U = rhoU/rho;
    U.correctBoundaryConditions();
    
    // Calculate enthalpy from rhoE
    const volScalarField Cp = thermo->Cp();
    const volScalarField Cv = thermo->Cv();
    
    // Alternative formulation, Felipe Alves Portela TU Delft
    e =(rhoE/rho - 0.5*magSqr(U));
    
    // Bound Internal Energy

    //~ dimensionedScalar eMin ( "eMin", e.dimensions(), thermo->EOSe(pMin.value(),TMin.value()) );
    //~ dimensionedScalar eMax ( "eMin", e.dimensions(), thermo->EOSe(pMax.value(),TMax.value()) );

    //~ boundMinMax(e, eMin, eMax);
    
    forAll(p,Celli)
    {
        p[Celli]=thermo->EOSP(rho[Celli],e[Celli]);
    }
    
    p.correctBoundaryConditions();

    // Correct thermodynamics
    thermo->correct();

    // Bound density
    //~ boundMinMax(rho, rhoMin, rhoMax);


    // Bound pressure
    //~ boundMinMax(p, pMin, pMax);
}
