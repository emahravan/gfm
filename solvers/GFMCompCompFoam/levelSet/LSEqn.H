    volScalarField f=psi;
    List<Vector<label> >& IJK=STMesh.IJK();
    const Vector<label>& ijkMax=STMesh.ijkMax(),ijkMin=STMesh.ijkMin();
    Info<<"Solving LS equation"<<endl;
#   include    "gatherPsi.H"   
    stWENO WENO(stPsi,STMesh,ijkMinLS,ijkMaxLS);
    const Vector<vector::components> DIRs(vector::X,vector::Y,vector::Z);
    const label DIRMax= STMesh.threeD()?2:1;
    List<scalar> V(6);

        forAll(IJK,P)
        {
            scalar dx=deltaX.value();
         
            Vector<label> ijkG=IJK[P];//Global
            
            Vector<label> ijkL=STMesh.localIndex(ijkG);//Local

            if(ijkG.x()<= ijkMaxLS.x() && ijkG.y()<=ijkMaxLS.y() && ijkG.z()<=ijkMaxLS.z() && 
                ijkG.x()>= ijkMinLS.x() && ijkG.y()>=ijkMinLS.y() && ijkG.z()>=ijkMinLS.z())//Level set is solved only around the interface to allow for non-uniform mesh away from the interface and to reduce computational cost. If you need to solve the whole domain ijkMinLS and ijkMaxLS should be the same as the whole mesh.
            {
                vector phi(0,0,0);

                for(label DIRCount=0; DIRCount<=DIRMax;DIRCount++)
                {
                    vector::components DIR=DIRs[DIRCount];
                    
                    if(U[P][DIR]>0)
                    {
                        WENO.findVMinus(V,ijkL,DIR);
                        phi[DIR]=WENO.calcWENOPhiMinus(V,deltaX.value());
                    }
            
                    else if(U[P][DIR]<0)
                    {

                        WENO.findVPlus(V,ijkL,DIR);        

                        phi[DIR]=WENO.calcWENOPhiPlus(V,deltaX.value());
                        
                    }
                }
            
                
                f[P]=-(U[P]&phi);
                
            }
        } 
