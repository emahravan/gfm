
{
    const List<Vector<label> >& IJK=STMesh.IJK();


#   include    "gatherPsi.H"

    forAll(IJK,P)
    {
        scalar dx=deltaX.value();
        scalar dy=deltaX.value();
        scalar dz=deltaX.value();
        
        Vector<label> ijkG=IJK[P];
        Vector<label> ijkL=STMesh.localIndex(IJK[P]);
        
        const label i=ijkL.x(),j=ijkL.y(),k=ijkL.z();
        const label iG=ijkG.x(),jG=ijkG.y(),kG=ijkG.z();

        if(iG<= ijkMaxLS.x()-1 && jG<=ijkMaxLS.y()-1 && kG<=ijkMaxLS.z()-1 && iG>=ijkMinLS.x()+1 && jG>=ijkMinLS.y()+1 && kG>=ijkMinLS.z()+1)
        {
            scalar psiX=( stPsi[i+1][j][k] - stPsi[i-1][j][k] ) / (2.*dx),
                   psiY=( stPsi[i][j+1][k] - stPsi[i][j-1][k] ) / (2.*dy),
                   psiZ=( stPsi[i][j][k+1] - stPsi[i][j][k-1] ) / (2.*dz);
                   
            scalar psiX2=psiX*psiX,
                   psiY2=psiY*psiY,
                   psiZ2=psiZ*psiZ;
            
            n1[P]=vector(psiX,psiY,psiZ)/Foam::sqrt(psiX2+psiY2+psiZ2);
        }
        
        else if(iG<= ijkMaxLS.x() && jG<=ijkMaxLS.y() && kG<=ijkMaxLS.z() && iG>=ijkMinLS.x() && jG>=ijkMinLS.y() && kG>=ijkMinLS.z())
        
        if(iG==ijkMaxLS.x() || jG==ijkMaxLS.y() || kG==ijkMaxLS.z() || iG==ijkMinLS.x() || jG==ijkMinLS.y() || kG==ijkMinLS.z())
        {
            scalar psiX=0,psiY=0,psiZ=0;
            
            if(iG == ijkMaxLS.x())
            {
                psiX=(-3.*stPsi[i][j][k]+4*stPsi[i-1][j][k]-stPsi[i-2][j][k])/(2*dx);
            }
            
            else if(iG == ijkMinLS.x())
            {
                psiX=(-3.*stPsi[i][j][k]+4*stPsi[i+1][j][k]-stPsi[i+2][j][k])/(2*dx);
            }
            else
            {
                psiX=(stPsi[i+1][j][k]-stPsi[i-1][j][k])/(2.*dx);
            }
            
            if(jG == ijkMaxLS.y())
            {
                psiY=(-3*stPsi[i][j][k]+4*stPsi[i][j-1][k]-stPsi[i][j-2][k])/(2.*dy);
            }
            else if(jG == ijkMinLS.y())
            {
                if(jG==0&&symmetry)
                {
                    psiY=(stPsi[i][j+1][k]-stPsi[i][j][k])/(2.*dy);
                }
                else
                {
                    psiY=(-3*stPsi[i][j][k]+4*stPsi[i][j+1][k]-stPsi[i][j+2][k])/(2.*dy);
                }
            }
            else
            {
                psiY=(stPsi[i][j+1][k]-stPsi[i][j-1][k])/(2.*dy);
            }
            
            
            if(kG == ijkMaxLS.z())
            {
                psiZ=(-3*stPsi[i][j][k]+4*stPsi[i][j][k-1]-stPsi[i][j][k-2])/(2.*dz);
            }
            else if(kG == ijkMinLS.z())
            {
                if(kG==0&&symmetry)
                {
                    psiZ=(stPsi[i][j][k+1]-stPsi[i][j][k])/(2.*dz);
                }
                else
                {
                    psiZ=(-3*stPsi[i][j][k]+4*stPsi[i][j][k+1]-stPsi[i][j][k+2])/(2.*dz);
                }
            }
            else
            {
                psiZ=(stPsi[i][j][k+1]-stPsi[i][j][k-1])/(2.*dz);
            }
            
            scalar psiX2=psiX*psiX,psiY2=psiY*psiY,psiZ2=psiZ*psiZ;            
            n1[P]=vector(psiX,psiY,psiZ)/Foam::sqrt(psiX2+psiY2+psiZ2);
        }
    }
}
