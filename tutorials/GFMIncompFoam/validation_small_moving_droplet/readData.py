#This file was originally written by Hakan Nilson, Chalmers university

import numpy as  np
import matplotlib.pyplot as plt

plt.rc('text', usetex=True)
plt.rc('font', family='Times New Roman')# If this does not work: sudo apt install msttcorefonts -qq   , then: rm ~/.cache/matplotlib -rf
# ~ plt.rc('size', size=10)# If this does not work: sudo apt install msttcorefonts -qq   , then: rm ~/.cache/matplotlib -rf

myMarker='X'
FedkiwMarker='o'

zoom=0.45

fig, (ax1) = plt.subplots(1, 1)
w, h = fig.get_size_inches()
fig.set_size_inches(w * zoom*1.5, h * zoom)


fig.tight_layout() # Or equivalently,  "plt.tight_layout()"

#write a function
fedkiw=np.loadtxt("PlotDigitizer/density_small_droplet")
mine=np.loadtxt("sets/5e-09/data_rho_p_pI_si_magU_magUI.xy")


for i in range(0,mine.shape[0]):
    if(mine[i,4]<=0):
        mine[i,1]=10
        mine[i,2]=mine[i,3]*10

for i in range(0,mine.shape[0]):
    if(mine[i,4]<=0):
        mine[i,5]=mine[i,6]#Compressible velocity equal to incompressible velocity for plotting purposes 

plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

plt.scatter(fedkiw[:,0], fedkiw[:,1], label='Fedkiw', color='black')
plt.plot(mine[:,0], mine[:,1], linestyle='-', fillstyle='full', marker=myMarker, label='This work 100' , color='black')

plt.xlabel(r'$x (m)$',fontsize=10)
plt.ylabel(r'$\rho (kg.m^{-3})$',fontsize=10)
plt.xlim(0,1e-5)
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

plt.legend(loc='upper left',fontsize=10)
plt.savefig('validationRho.eps')



fig, (ax1) = plt.subplots(1, 1)
w, h = fig.get_size_inches()
fig.set_size_inches(w * zoom*1.5, h * zoom)


plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

fedkiw=np.loadtxt("PlotDigitizer/pressure_small_droplet")
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))

plt.scatter(fedkiw[:,0], fedkiw[:,1], label='Fedkiw', color='black')
plt.plot(mine[:,0], mine[:,2], linestyle='-', fillstyle='full', marker=myMarker, label='This work 100' , color='black')
plt.legend(loc='upper right',fontsize=10)
plt.xlim(0,1e-5)

plt.savefig('validationPressure.eps')


fig, (ax1) = plt.subplots(1, 1)
w, h = fig.get_size_inches()
fig.set_size_inches(w * zoom*1.5, h * zoom)



fedkiw=np.loadtxt("PlotDigitizer/velocity_small")
plt.scatter(fedkiw[:,0], fedkiw[:,1], label='Fedkiw', color='black')
plt.plot(mine[:,0], mine[:,5], linestyle='-', fillstyle='full', marker=myMarker, label='This work 100' , color='black')

plt.xlabel(r'$x (m)$',fontsize=10)
plt.ylabel(r'$u (m/s)$',fontsize=10)

plt.xlim(0,1e-5)
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))


plt.legend(loc='upper middle',fontsize=10)

plt.savefig('validationVelocity.eps')


plt.show() 
