As=[40e3,60e3,80e3]
FREQs=[200e3]#,400e3,800e3,1200e3]
Nxs=[610,720,1480,2200]
NDroplets=[400,400,800,1200]
iFREQ=-1
for FREQ in FREQs:
    iFREQ=iFREQ+1
    for A in As:
    
        Nx=Nxs[iFREQ] #total number of points in x direction
        NDroplet=NDroplets[iFREQ] #Number of segments in LDroplet

        DELTAT=0.5e-8

        NPROCESSORS=4
        NXPROCESSORS=4
        NYPROCESSORS=1

        ENDTIME=0.001
        LastRunEndTime=0.001
        WRITEINTERVAL=LastRunEndTime/500.


        if FREQ==200e3:
            DELTAT=1e-8
            LastRunEndTime=0.0005
            WRITEINTERVAL=LastRunEndTime/250.
            if A==60e3:
                LastRunEndTime=0.00025
                WRITEINTERVAL=LastRunEndTime/250.
            if A==80e3:
                LastRunEndTime=0.00025
                WRITEINTERVAL=LastRunEndTime/250.
            


        if FREQ==400e3:
            DELTAT=1e-8
            LastRunEndTime=0.0005
            WRITEINTERVAL=LastRunEndTime/250.
            if A==60e3:
                LastRunEndTime=0.00025
                WRITEINTERVAL=LastRunEndTime/250.
            if A==80e3:
                LastRunEndTime=0.00025
                WRITEINTERVAL=LastRunEndTime/250.
                
        if FREQ==800e3:
            DELTAT=1e-8
            LastRunEndTime=0.00020
            WRITEINTERVAL=LastRunEndTime/200.
                
        if FREQ==1200e3:
            DELTAT=1e-8
            LastRunEndTime=0.0002
            WRITEINTERVAL=LastRunEndTime/200.


        R=0.5e-3
        import numpy as np


        T=300
        p=1e6
        gasConstant=287
        specificGasRatio=1.4
        c=(gasConstant*298*specificGasRatio)**0.5
        landa=c/FREQ
        k=2.*np.pi/landa

        p0=20e-6

        sigma=0.073

        rho=p/(gasConstant*T)

        Ba=1.3


        kR=R*k
        #A=p0*10**(L_p/20.)
        Ba=9*A*A*R/(sigma*rho*c*c)

        L_p=20*np.log10(A/p0/2.**0.5)

        L=8.*R#Domain Length, selected after tunning
        LDroplet=R*3. #Length of the region around the doplet with uniform grid (Droplet Region)

        print ('LDroplet=',LDroplet, 'NDroplet=',NDroplet, 'Nx=',Nx)

        print ("SLP=",L_p,"    A=",A,"    R=",R,"    R*2.5=",R*2.5,"    Ba=",Ba,"    kR=",kR,"    L=",L,"     (L-LDroplet)/landa=",(L-LDroplet)/landa)

        # ================================================================== #
        # ================================================================== #
        # ================================================================== #

        import sys
        import os


        LyLxRatio=1. #Ratio of domain hight to widgth

        Lx=L
        Ly=LyLxRatio*Lx/2.


        LTransducerLxRatio=1. #Ratio of domain hight to widgth
        LyLxRatio=1. #Ratio of domain hight to widgth
        LzLxRatio=LyLxRatio*0.05 #Ratio of domain hight to widgth
        LzLxRatioTrans=LTransducerLxRatio*0.05

        Ny=int(Nx*float(LyLxRatio)/2) #total number of points in x direction
        print ("Ny=",Ny)
        NTrans=int(LTransducerLxRatio/LyLxRatio*Ny)
        NSide =Ny-NTrans

        LTransducer=LTransducerLxRatio*Lx/2.
        LSide=Ly-LTransducer


        dDroplet=LDroplet/NDroplet #segment length in 

        ###########################################################################################################
        ###########################################################################################################
        ###########################################################################################################

        case=str(FREQ)+"-"+str(A)
        os.system('rm -r '+case)
        os.system('cp -r Sample '+case)
        os.system('cp prepareCases.py '+case)

        Ny=int(Nx*LyLxRatio/2) #total number of points in x direction
            
        NTrans=int(LTransducerLxRatio*Nx/2)

        NSide =Ny-NTrans

        LTransducer=LTransducerLxRatio*Lx/2.
        LSide=Ly-LTransducer

        XgrL2=LDroplet/Lx
        XgrL1=(1-XgrL2)/2.
        XgrL3=(1-XgrL2)/2.

        XgrN1=(Nx-NDroplet)/2
        XgrN2=NDroplet
        XgrN3=(Nx-NDroplet)/2

        if(XgrN1+XgrN2+XgrN3==Nx-1):
            XgrN2=XgrN2+1 #if Nx-NDroplet becomes odd this is needed

        error=10
        a=dDroplet
        XgrG3=1.001
        r=XgrG3

        #~ print "r=",r,"    XgrG1=",XgrG1,"   XgrN1=",XgrN1,"   L=",L,"    a=",a

        while error>1e-8:
            #~ XgrG1=1-a/L*(1-r**(XgrN1+1))
            XgrG3=(1-(XgrL3*Lx)/a*(1-r))**(1./(XgrN3+1))
            
            error=abs(XgrG3-r)
            r=XgrG3

        XgrG3=r**XgrN3
        XgrG1=1./XgrG3
        XgrG2=1

        print ("dDroplet=",dDroplet,"    dDroplet/XgrG3=",dDroplet/XgrG3,"    dDroplet*XgrG3=",dDroplet*XgrG3,"    dLanda requiered=",landa/60.,"    landa=",landa,"    D=",R*2.)

        XgrN1=XgrN1*1.0/Nx
        XgrN2=XgrN2*1.0/Nx
        XgrN3=XgrN3*1.0/Nx

        YgrL1=LDroplet/2./Ly
        YgrL2=(1-YgrL1)

        YgrN1=NDroplet/2
        YgrN2=(Ny-YgrN1)# initially we assume thet the second part consist whole y length, and the expansion ratio, r, is obtained for this length

        error=10
        a=dDroplet
        YgrG2=1.001
        r=YgrG2

        while error>1e-8:
            
            YgrG2=(1-(YgrL2*Ly)/a*(1-r))**(1./(YgrN2+1))
            
            error=abs(YgrG2-r)
            r=YgrG2
            #~ print "r=",r,"   YgrN2=",YgrN2,"   L=",L,"    a=",a

        #Values must be corrected to consider the Transducer part
        YgrN2=int(np.log(1-(Ly-LSide-LDroplet/2.)*(1-r)/a)/np.log(r)-1+0.5)#N now is corrected for the second part of transducer
        YgrL1=LDroplet/2./LTransducer
        YgrL2=(1-YgrL1)

        NTrans=Ny

        print ('YgrN2=',YgrN2,'NTrans=',NTrans,'NSide=',NSide)

        YgrG2=r**YgrN2
        YgrG3=r**Ny/YgrG2
        YgrG1=1

        YgrN1=YgrN1*1.0/NTrans
        YgrN2=YgrN2*1.0/NTrans


        print ('L=',L,'Ly=',Ly,'    LDroplet/2=',LDroplet/2,'LTransducer=',LTransducer)

        print ('YgrL1=',YgrL1,'YgrL2=',YgrL2)


        blockMeshDictOrig=case+"/system/blockMeshDict.orig"
        blockMeshDict=case+"/system/blockMeshDict"

        os.system('sed -e s/LconvertToMeters/'+str(L/2.)+'/g  '+blockMeshDictOrig+' > '+blockMeshDict)

        os.system('sed -i s/Nx/'+str(Nx)+'/g  '+blockMeshDict)

        os.system('sed -i s/NTrans/'+str(Ny)+'/g  '+blockMeshDict)
        os.system('sed -i s/NSide/'+str(NSide)+'/g  '+blockMeshDict)

        os.system('sed -i s/LyLxRatio/'+str(LyLxRatio)+'/g  '+blockMeshDict)
        os.system('sed -i s/LTransducerLxRatio/'+str(LTransducerLxRatio)+'/g  '+blockMeshDict)
        os.system('sed -i s/LzLxRatioTrans/'+str(LzLxRatioTrans)+'/g  '+blockMeshDict)
        os.system('sed -i s/LzLxRatio/'+str(LzLxRatio)+'/g  '+blockMeshDict)


        os.system('sed -i s/XgrN1/'+str(XgrN1)+'/g  '+blockMeshDict)
        os.system('sed -i s/XgrN2/'+str(XgrN2)+'/g  '+blockMeshDict)
        os.system('sed -i s/XgrN3/'+str(XgrN3)+'/g  '+blockMeshDict)

        os.system('sed -i s/XgrL1/'+str(XgrL1)+'/g  '+blockMeshDict)
        os.system('sed -i s/XgrL2/'+str(XgrL2)+'/g  '+blockMeshDict)
        os.system('sed -i s/XgrL3/'+str(XgrL3)+'/g  '+blockMeshDict)

        os.system('sed -i s/XgrG1/'+str(XgrG1)+'/g  '+blockMeshDict)
        os.system('sed -i s/XgrG2/'+str(XgrG2)+'/g  '+blockMeshDict)
        os.system('sed -i s/XgrG3/'+str(XgrG3)+'/g  '+blockMeshDict)


        os.system('sed -i s/YgrN1/'+str(YgrN1)+'/g  '+blockMeshDict)
        os.system('sed -i s/YgrN2/'+str(YgrN2)+'/g  '+blockMeshDict)

        os.system('sed -i s/YgrL1/'+str(YgrL1)+'/g  '+blockMeshDict)
        os.system('sed -i s/YgrL2/'+str(YgrL2)+'/g  '+blockMeshDict)

        os.system('sed -i s/YgrG1/'+str(YgrG1)+'/g  '+blockMeshDict)
        os.system('sed -i s/YgrG2/'+str(YgrG2)+'/g  '+blockMeshDict)
        os.system('sed -i s/YgrG3/'+str(YgrG3)+'/g  '+blockMeshDict)


        os.system('blockMesh -case '+ case+' > '+case+'/log.blockMesh')

        pressureOrig=case+"/0/p.orig"
        pressure=case+"/0/p"

        os.system('sed -e s/AMP/'+str(A)+'/g  '+pressureOrig+' > '+pressure)
        os.system('sed -i s/FREQ/'+str(FREQ)+'/g  '+pressure)

        transportPropertiesOrig=case+"/constant/transportProperties.orig"
        transportProperties=case+"/constant/transportProperties"
        os.system('sed -e s/DELTAX/'+str(dDroplet)+'/g  '+transportPropertiesOrig+' > '+transportProperties)
        os.system('sed -i s/SOLVEDBNS/yes/g  '+transportProperties)
        os.system('sed -i s/RADIUS/'+str(R)+'/g  '+transportProperties)

        IMAXLS=Nx/2+int(NDroplet/2)
        IMINLS=Nx/2-int(NDroplet/2)
        JMAXLS=int(NDroplet/2)
        JMINLS=0

        os.system('sed -i s/IMAXLS/'+str(IMAXLS)+'/g  '+transportProperties)
        os.system('sed -i s/IMINLS/'+str(IMINLS)+'/g  '+transportProperties)
        os.system('sed -i s/JMAXLS/'+str(JMAXLS)+'/g  '+transportProperties)
        os.system('sed -i s/JMINLS/'+str(JMINLS)+'/g  '+transportProperties)


        NStMeshOrig=case+"/constant/NStMesh.orig"
        NStMesh=case+"/constant/NStMesh"
        os.system('sed -e s/Nx/'+str(Nx)+'/g  '+NStMeshOrig+' > '+NStMesh)
        os.system('sed -i s/Ny/'+str(Ny)+'/g  '+NStMesh)

        controlDictOrig=case+"/system/controlDict.orig"
        controlDict=case+"/system/controlDict"
        os.system('sed -e s/DELTAT/'+str(DELTAT)+'/g  '+controlDictOrig+' > '+controlDict)
        os.system('sed -i s/WRITEINTERVAL/'+str(WRITEINTERVAL)+'/g  '+controlDict)
        os.system('sed -i s/ENDTIME/'+str(5.)+'/g  '+controlDict)

        os.system('blockMesh -case '+ case+' > '+case+'/log.blockMesh')

        decomposeParDictOrig=case+"/system/decomposeParDict.orig"
        decomposeParDict=case+"/system/decomposeParDict"
        os.system('sed -e s/NPROCESSORS/'+str(NPROCESSORS)+'/g  '+decomposeParDictOrig+' > '+decomposeParDict)
        os.system('sed -i s/NXPROCESSORS/'+str(NXPROCESSORS)+'/g  '+decomposeParDict)
        os.system('sed -i s/NYPROCESSORS/'+str(NYPROCESSORS)+'/g  '+decomposeParDict)

        os.system('blockMesh -case '+ case+' > '+case+'/log.blockMesh')

