#This file was originally written by Hakan Nilson, Chalmers university

import numpy as  np
import matplotlib.pyplot as plt


              
plt.rc('text', usetex=True)
plt.rc('font', family='serif')#Use for latex format

#write a function
fedkiw=np.loadtxt("PlotDigitizer/Density")
mine=np.loadtxt("sets/0.00125/data_rho_p_pI_si_magU_magUI.xy")
mine100=np.loadtxt("../validation-debug100/sets/0.00125/data_rho_p_pI_si_magU_magUI.xy")
mine50=np.loadtxt("../validation-debug-50/sets/0.00125/data_rho_p_pI_si_magU_magUI.xy")

for i in range(0,mine.shape[0]):
    if(mine[i,4]<=0):
        mine[i,1]=10
        mine[i,2]=mine[i,3]*10
p=mine100[:,2].copy()
for i in range(0,mine100.shape[0]):
    if(mine100[i,4]<=0):
        mine100[i,1]=10
        mine100[i,2]=mine100[i,3]*10

for i in range(0,mine50.shape[0]):
    if(mine50[i,4]<=0):
        mine50[i,1]=10
        mine50[i,2]=mine50[i,3]*10

for i in range(0,mine100.shape[0]):
    if(mine100[i,4]<=0):
        mine100[i,1]=10

for i in range(0,mine50.shape[0]):
    if(mine50[i,4]<=0):
        mine50[i,1]=10

for i in range(0,mine.shape[0]):
    if(mine[i,4]<=0):
        mine[i,5]=mine[i,6]#Compressible velocity equal to incompressible velocity for plotting purposes 

for i in range(0,mine100.shape[0]):
    if(mine100[i,4]<=0):
        mine100[i,5]=mine100[i,6]#Compressible velocity equal to incompressible velocity for plotting purposes 

plt.scatter(fedkiw[:,0], fedkiw[:,1], label='Fedkiw', color='black')
plt.plot(mine[:,0], mine[:,1], linestyle='-', label='This work 200', color='black')
plt.plot(mine100[:,0], mine100[:,1], linestyle='-', label='This work 100', color='blue')
plt.plot(mine50[:,0], mine50[:,1], linestyle='-', label='This work 50', color='red')

plt.xlabel(r'$x$',fontsize=20)
plt.ylabel(r'$\rho$',fontsize=20)
plt.legend(loc='upper right',fontsize=14)

plt.savefig('Density.eps')
plt.savefig('Density.jpg')


plt.figure()

fedkiw=np.loadtxt("PlotDigitizer/Pressure")

plt.scatter(fedkiw[:,0], fedkiw[:,1]*1e5, label='Fedkiw', color='black')
plt.plot(mine[:,0], mine[:,2], linestyle='-', label='This work 200' , color='black')
plt.plot(mine100[:,0], mine100[:,2], label='This work 100' , color='blue')
plt.plot(mine50[:,0], mine50[:,2], label='This work 50' , color='red')
# ~ plt.scatter(mine100[:,0], p, label='p' , color='red')
plt.legend(loc='upper right',fontsize=14)
plt.savefig('Pressure.eps')
plt.savefig('Pressure.jpg')


plt.figure()

fedkiw=np.loadtxt("PlotDigitizer/velocity")

plt.scatter(fedkiw[:,0], fedkiw[:,1], label='Fedkiw', color='black')
plt.plot(mine[:,0], mine[:,5], linestyle='-', label='This work 200' , color='black')
plt.plot(mine100[:,0], mine100[:,5], linestyle='-', label='This work 100' , color='blue')
plt.plot(mine50[:,0], mine50[:,5], linestyle='-', label='This work 50' , color='red')
plt.legend(loc='upper right',fontsize=14)
plt.savefig('velocity.eps')
plt.savefig('velocity.jpg')

# ~ plt.show() 
